const fs = require('fs');
const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const ROOT_DIR = fs.realpathSync(process.cwd());
const isDev = process.env.NODE_ENV !== 'production';
const analyze = process.argv.includes('--analyze');

function pathResolve(...args) {
  return path.resolve(ROOT_DIR, ...args);
}

function getName(ext) {
  return `[name].[${isDev ? 'hash:8' : 'contenthash'}].bundle.${ext}`;
}

module.exports = {
  mode: isDev ? 'development' : 'production',
  entry: {
    main: './src/index.jsx',
  },
  output: {
    filename: getName('js'),
    path: pathResolve('dist'),
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.png', 'scss'],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HTMLWebpackPlugin({
      template: pathResolve('public/index.html'),
    }),

    new MiniCssExtractPlugin({
      filename: 'css/[name].css',
    }),
    new CopyWebpackPlugin([
      {
        from: pathResolve('src/assets/img'),
        to: pathResolve('dist/img'),
      },
    ]),
    analyze && new BundleAnalyzerPlugin(),
  ].filter(Boolean),
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: [/node_modules/],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
          },
        },
      },
      {
        test: /\.s[ac]ss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(png|jpe?g|svg|gif|ico)$/,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
      {
        test: /\.(ttf|woff|woff2|eot)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'fonts',
            },
          },
        ],
      },
    ],
  },
  devServer: {
    port: '3000',
    open: true,
  },
};
