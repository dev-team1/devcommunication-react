import React from 'react';
import renderer from 'react-test-renderer';
import { Dignity } from '../Dignity';

describe('Dignity component', () => {
  test('Matches the snapshot', () => {
    const dignity = renderer.create(<Dignity />);
    expect(dignity.toJSON()).toMatchSnapshot();
  });
});
