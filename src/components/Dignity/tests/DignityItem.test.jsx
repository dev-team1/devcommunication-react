import React from 'react';
import renderer from 'react-test-renderer';
import { DignityItem } from '../DignityItem';

describe('DignityItem component', () => {
  test('Matches the snapshot', () => {
    const dignityItem = renderer.create(<DignityItem />);
    expect(dignityItem.toJSON()).toMatchSnapshot();
  });
});
