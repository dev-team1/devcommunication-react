import React from 'react';
import PropTypes from 'prop-types';

export const DignityItem = (props) => {
  return (
    <div className="dignity_item">
      <img src="img/plus.png" alt="plus" className="plus" />
      <div className="dignity_description">{props.message}</div>
    </div>
  );
};

DignityItem.propTypes = {
  message: PropTypes.string,
};
