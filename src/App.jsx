import React from 'react';

import { Dignity } from './components/Dignity/Dignity';

export const App = () => {
  return (
    <div className="App">
      <Dignity />
    </div>
  );
};
